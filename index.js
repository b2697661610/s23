
let trainer ={};
	trainer.name= "Ash Ketchum";
	trainer.age=18;
	trainer.pokemon=["Pikachu,Charizard,Squirtle,Bulbasaur"];
	trainer.friends={ hoenn:["May","Max"],
					kanto:["Brock","Misty"]}
						
	trainer.talk= function(){
	console.log("Pikachu! I choose you");}
	
console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);
console.log(trainer["pokemon"]);
console.log("Result of square bracket notation:");

console.log("Result of talk method");
trainer.talk();


function Pokemon(name,level) {
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	this.tackle = function(target) {
console.log( this.name + " tackled " + target.name);

	   
	    target.health -= this.attack;
	    console.log( target.name +" health is now reduced to " + target.health);
	    
	    
	    if (target.health <= 0) {
	   	        target.faint()
	    }};

	
	this.faint = function(){
	    console.log( this.name +" fainted");
	}}

let pikachu = new Pokemon("Pikachu",15);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 10);
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 50);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);

pikachu.tackle(mewtwo);
console.log(mewtwo);






